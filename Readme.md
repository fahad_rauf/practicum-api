**Running the code locally**

Use this command

`gradle bootRun`

**Making a war file**

go inside the code directory and use this command

`gradle war`

**Deploying new war file on server**

ssh into the server and run this command to delete the deployed version

`rm -Rf /var/lib/tomcat7/webapps/*`

go into the folder where war file is placed. In the project it is placed in `/build/libs` directory

`scp api-0.5.0.war root@159.203.17.246:/var/lib/tomcat7/webapps/ROOT.war`

here "api-0.5.0.war" is the war file name

**Script to add new courses**

_1- SSH into the server_

_2- Open mysql_ 

`mysql -u root -p`

enter password "123456"

_3- script to add course given below_

//course addition scripts

`insert into coachingsite.t_course (category, content_info, course_id, description, outline, title) values 
("PMP", "Project Management Framework PMBOK 5 th Edition, and Professional & Social Responsibility", "PMP","Project Management Professional (PMP®) Exam Preparation Course is a hands-on program designed to equip students with the fundamentals in project management, based on the Guideto the Project Management Body of Knowledge (PMBOK®), 5 th Edition. The program has beendesigned for project managers and team members who desire a better understanding of theproject management processes and where they fit according to the PMBOK®.</br></br>The participants will learn about the ten knowledge areas and five process groups of the PMBOK®5 th edition, and how to apply it to your work. It is an experiential learning process as you build onthe participants’ knowledge base with the right project management processes from initiation,Planning, executing, monitoring and controlling, and finally the closing of a project. Theknowledge gained from this program will adequately prepare the participants for the PMP®exams.</br></br>In this course, the participants will also go through project management exams in order toconsolidate their knowledge. The simulation exams will enable the participants to do their owngap analysis to see how they should focus on their PMP® exam preparation in order to besuccessful.</br></br>The Practicum takes pride on our method of delivery for our entire course curriculum. Our classesare not a lecture, and our Instructors are not mere tutors. The transfer of knowledge to ourstudents is by mode of a very interactive and hands-on delivery within a practical learningenvironment. Students are expected to be active and contribute in class. In addition, theparticipants will bring home with them not only a well-based foundation of project managementknowledge, but also an extensive complete package of course materials, which includes thefollowing:</br></br>","The course is a combination of lectures, group work, test examples, exam tips, actual timed tests, and independent study.","Project Management Professional (PMP®) Exam Preparation Course");`

//module addition scripts

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-1", "Project Management Process Groups", "Initiating Process Group");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-1", "Project Management Process Groups", "Planning Process Group");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-1", "Project Management Process Groups", "Executing Process Group");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-1", "Project Management Process Groups", "Monitoring & Controlling Process Group");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-1", "Project Management Process Groups", "Closing Process Group");`


`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Management Knowledge Areas");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Scope Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Time Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Cost Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Quality Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Human Resource Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Communication Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Risk Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Procurement Management");`

`insert into coachingsite.t_module (module_id, module_title, sub_module_title) values
("pmp-2", "Project Management Knowledge Areas", "Project Stakeholder Management");`

//course content addition script

`insert into coachingsite.t_course_content (course_id, module_id) values
("PMP", "pmp-1");`

`insert into coachingsite.t_course_content (course_id, module_id) values
("PMP", "pmp-2");`

//course fee addition scripts

`insert into coachingsite.t_course_fee (course_id, fee, fee_currency, fee_type) values
("PMP", 200, "CAD", "Course Fee");`

`insert into coachingsite.t_course_fee (course_id, fee, fee_currency, fee_type) values
("PMP", 555, "CAD", "Exam Fee");`

`insert into coachingsite.t_course_fee (course_id, fee, fee_currency, fee_type) values
("PMP", 405, "CAD", "PMI Members Fee");`

//course deliverable addition scripts

`insert into coachingsite.t_course_deliverable (course_id, deliverable) values
("PMP", "PMP® Exam Questions Booklets");`

`insert into coachingsite.t_course_deliverable (course_id, deliverable) values
("PMP", "Resource Book (Lecture Notes)");`

`insert into coachingsite.t_course_deliverable (course_id, deliverable) values
("PMP", "Participation Certificate");`

`insert into coachingsite.t_course_deliverable (course_id, deliverable) values
("PMP", "35 Professional Development Units (PDUs)");`

//course schedule addition scripts

`insert into coachingsite.t_course_schedule (course_id, end_time, instructor, location, start_time) values
("PMP", "2018-02-22 20:10:00", "Tom David", "Texas", "2017-11-22 04:10:00");`

**To change stripe to production**

Open this file

`practicum-api/src/main/resources/config/application.properties`

change the stripe secret key here

`spring.stripe.secretKey = sk_test_8SgZSccgEZnj8mJczFvAFmHD`

**Email SMPTP settings**

`Open this file`

change the SMTP settings

`spring.mail.host=smtp.gmail.com`

`spring.mail.port=587`

`spring.mail.username=practicumcanada@gmail.com`

`spring.mail.password=practicum123`

**MYSQL settings**

`spring.datasource.username=root`

`spring.datasource.password=123456`



