package com.choachingsite.api.courseschedule.port;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class CourseScheduleResource
{
    @Autowired
    private CourseScheduleResource()
    {}
}
