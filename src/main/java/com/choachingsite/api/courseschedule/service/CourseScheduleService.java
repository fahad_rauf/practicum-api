package com.choachingsite.api.courseschedule.service;

import com.choachingsite.api.courseschedule.repository.CourseScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CourseScheduleService
{
    private final CourseScheduleRepository courseScheduleRepository;

    @Autowired
    public CourseScheduleService(CourseScheduleRepository courseScheduleRepository)
    {
        this.courseScheduleRepository = courseScheduleRepository;
    }
}
