package com.choachingsite.api.courseschedule.repository;

import com.choachingsite.api.courseschedule.domain.CourseSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseScheduleRepository extends JpaRepository<CourseSchedule, Long>
{
    CourseSchedule findOneByCourseId(String courseId);
}
