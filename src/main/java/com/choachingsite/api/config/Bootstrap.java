package com.choachingsite.api.config;

import com.choachingsite.api.course.domain.CourseContent;
import com.choachingsite.api.course.domain.CourseDeliverable;
import com.choachingsite.api.course.domain.CourseFee;
import com.choachingsite.api.course.domain.Module;
import com.choachingsite.api.course.repository.CourseContentRepository;
import com.choachingsite.api.course.repository.CourseDeliverableRepository;
import com.choachingsite.api.course.repository.CourseFeeRepository;
import com.choachingsite.api.course.repository.CourseRepository;
import com.choachingsite.api.course.domain.Course;
import com.choachingsite.api.course.repository.ModuleRepository;
import com.choachingsite.api.courseschedule.domain.CourseSchedule;
import com.choachingsite.api.courseschedule.repository.CourseScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class Bootstrap
{
    private final CourseRepository courseRepository;
    private final CourseDeliverableRepository courseDeliverableRepository;
    private final CourseFeeRepository courseFeeRepository;
    private final CourseContentRepository courseContentRepository;
    private final CourseScheduleRepository courseScheduleRepository;
    private final ModuleRepository moduleRepository;

    @Autowired
    public Bootstrap(CourseRepository courseRepository,
                     CourseDeliverableRepository courseDeliverableRepository,
                     CourseFeeRepository courseFeeRepository,
                     CourseContentRepository courseContentRepository,
                     CourseScheduleRepository courseScheduleRepository, ModuleRepository moduleRepository)
    {
        this.courseRepository = courseRepository;
        this.courseDeliverableRepository = courseDeliverableRepository;
        this.courseFeeRepository = courseFeeRepository;
        this.courseContentRepository = courseContentRepository;
        this.courseScheduleRepository = courseScheduleRepository;
        this.moduleRepository = moduleRepository;
    }

    public void init()
    {
        createSampleCourses();
    }

    private void createSampleCourses()
    {
        /*if (courseRepository.findCourseByCourseId("PMP") == null)
        {
            Course course1 = Course.newBuilder()
                    .description("Project Management Professional (PMP®) Exam Preparation Course is a hands-on program " +
                            "designed to equip students with the fundamentals in project management, based on the Guide" +
                            "to the Project Management Body of Knowledge (PMBOK®), 5 th Edition. The program has been" +
                            "designed for project managers and team members who desire a better understanding of the" +
                            "project management processes and where they fit according to the PMBOK®.</br></br>" +
                            "The participants will learn about the ten knowledge areas and five process groups of the PMBOK®" +
                            "5 th edition, and how to apply it to your work. It is an experiential learning process as you build on" +
                            "the participants’ knowledge base with the right project management processes from initiation," +
                            "Planning, executing, monitoring and controlling, and finally the closing of a project. The" +
                            "knowledge gained from this program will adequately prepare the participants for the PMP®" +
                            "exams.</br></br>" +
                            "In this course, the participants will also go through project management exams in order to" +
                            "consolidate their knowledge. The simulation exams will enable the participants to do their own" +
                            "gap analysis to see how they should focus on their PMP® exam preparation in order to be" +
                            "successful.</br></br>" +
                            "The Practicum takes pride on our method of delivery for our entire course curriculum. Our classes" +
                            "are not a lecture, and our Instructors are not mere tutors. The transfer of knowledge to our" +
                            "students is by mode of a very interactive and hands-on delivery within a practical learning" +
                            "environment. Students are expected to be active and contribute in class. In addition, the" +
                            "participants will bring home with them not only a well-based foundation of project management" +
                            "knowledge, but also an extensive complete package of course materials, which includes the" +
                            "following:</br></br>")
                    .category("PMP")
                    .courseId("PMP")
                    .contentInfo("Project Management Framework PMBOK 5 th Edition, and Professional & Social Responsibility")
                    .outline("The course is a combination of lectures, group work, test examples, exam tips, actual timed tests, and independent study.")
                    .title("Project Management Professional (PMP®) Exam Preparation Course")
                    .build();

            course1 = courseRepository.save(course1);

            Module modulePmp1 = Module.newBuilder()
                    .moduleId("pmp-1")
                    .moduleTitle("Project Management Process Groups")
                    .subModuleTitle("Initiating Process Group")
                    .build();
            moduleRepository.save(modulePmp1);

            modulePmp1 = Module.newBuilder()
                    .moduleId("pmp-1")
                    .moduleTitle("Project Management Process Groups")
                    .subModuleTitle("Planning Process Group")
                    .build();
            moduleRepository.save(modulePmp1);

            modulePmp1 = Module.newBuilder()
                    .moduleId("pmp-1")
                    .moduleTitle("Project Management Process Groups")
                    .subModuleTitle("Executing Process Group")
                    .build();
            moduleRepository.save(modulePmp1);

            modulePmp1 = Module.newBuilder()
                    .moduleId("pmp-1")
                    .moduleTitle("Project Management Process Groups")
                    .subModuleTitle("Monitoring & Controlling Process Group")
                    .build();
            moduleRepository.save(modulePmp1);

            modulePmp1 = Module.newBuilder()
                    .moduleId("pmp-1")
                    .moduleTitle("Project Management Process Groups")
                    .subModuleTitle("Closing Process Group")
                    .build();
            moduleRepository.save(modulePmp1);

            Module modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Integration Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Scope Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Time Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Cost Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Quality Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Human Resource Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Communication Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Risk Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Procurement Management")
                    .build();
            moduleRepository.save(modulePmp2);

            modulePmp2 = Module.newBuilder()
                    .moduleId("pmp-2")
                    .moduleTitle("Project Management Knowledge Areas")
                    .subModuleTitle("Project Stakeholder Management")
                    .build();
            moduleRepository.save(modulePmp2);

            CourseContent courseContent1 = CourseContent.newBuilder()
                    .courseId(course1.getCourseId())
                    .moduleId("pmp-1")
                    .build();
            courseContentRepository.save(courseContent1);

            CourseContent courseContent2 = CourseContent.newBuilder()
                    .courseId(course1.getCourseId())
                    .moduleId("pmp-2")
                    .build();
            courseContentRepository.save(courseContent2);

            CourseFee courseFee1 = CourseFee.newBuilder()
                    .courseId(course1.getCourseId())
                    .fee(200.0)
                    .feeCurrency("CAD")
                    .feeType("Course Fee")
                    .build();
            courseFee1 = courseFeeRepository.save(courseFee1);

            CourseFee courseFee2 = CourseFee.newBuilder()
                    .courseId(course1.getCourseId())
                    .fee(555.0)
                    .feeCurrency("CAD")
                    .feeType("Exam Fee")
                    .build();
            courseFee2 = courseFeeRepository.save(courseFee2);

            CourseFee courseFee3 = CourseFee.newBuilder()
                    .courseId(course1.getCourseId())
                    .fee(405.0)
                    .feeCurrency("CAD")
                    .feeType("PMI Members Fee")
                    .build();
            courseFee3 = courseFeeRepository.save(courseFee3);

            CourseDeliverable courseDeliverable1 = CourseDeliverable.newBuilder()
                    .courseId(course1.getCourseId())
                    .deliverable("PMP® Exam Questions Booklets")
                    .build();
            courseDeliverableRepository.save(courseDeliverable1);

            courseDeliverable1 = CourseDeliverable.newBuilder()
                    .courseId(course1.getCourseId())
                    .deliverable("Resource Book (Lecture Notes)")
                    .build();
            courseDeliverableRepository.save(courseDeliverable1);

            courseDeliverable1 = CourseDeliverable.newBuilder()
                    .courseId(course1.getCourseId())
                    .deliverable("Participation Certificate")
                    .build();
            courseDeliverableRepository.save(courseDeliverable1);

            courseDeliverable1 = CourseDeliverable.newBuilder()
                    .courseId(course1.getCourseId())
                    .deliverable("35 Professional Development Units (PDUs)")
                    .build();
            courseDeliverableRepository.save(courseDeliverable1);

            CourseSchedule courseSchedule1 = CourseSchedule.newBuilder()
                    .courseId(course1.getCourseId())
                    .startTime(LocalDateTime.of(2017, 11, 22, 4, 10))
                    .endTime(LocalDateTime.of(2018, 2, 22, 20, 10))
                    .instructor("Tom David")
                    .location("Texas")
                    .build();
            courseSchedule1 = courseScheduleRepository.save(courseSchedule1);
        }*/
    }
}
