package com.choachingsite.api.user.repository;

import org.springframework.data.repository.CrudRepository;

import com.choachingsite.api.user.domain.User;

public interface UserRepository extends CrudRepository<User, Long>
{

}
