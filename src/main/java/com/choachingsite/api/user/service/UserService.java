package com.choachingsite.api.user.service;

import com.choachingsite.api.user.repository.UserRepository;
import com.choachingsite.api.user.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserService
{
    private UserRepository userRepository;

    @Autowired
    public UserService (UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    public Long createUser (User user)
    {
        return userRepository.save(user).getId();
    }
}
