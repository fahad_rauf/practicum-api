package com.choachingsite.api.user.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_USER")
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = User.Builder.class)
public class User
{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

	@Column(name = "first_name", nullable = false)
	private String firstName;

	@Column(name = "last_name", nullable = false)
	private String lastName;

	@Column(name = "email", nullable = false)
    private String email;

	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;

	@Column(name = "address")
    private String address;

	@Column(name = "city")
	private String city;

	@Column(name = "post_code")
    private String postalCode;

	@lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
	private User(Long id, String firstName, String lastName, String phoneNumber, String email, String address, String city, String postalCode)
	{
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.address = address;
		this.city = city;
		this.postalCode = postalCode;
	}

	@JsonPOJOBuilder(withPrefix = "")
	public static class Builder
	{
	}
}

