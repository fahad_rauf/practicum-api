package com.choachingsite.api.user.port;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class UserResource
{
	@Autowired
	private UserResource ()
	{}
}
