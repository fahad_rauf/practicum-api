package com.choachingsite.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.TimeZone;

@SpringBootApplication
public class Application
{
	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	private Environment environment;

	@Autowired
	public Application(Environment environment)
	{
		this.environment = environment;
	}

	@PostConstruct
	public void initApplication()
	{
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

		if (environment.getActiveProfiles().length == 0)
		{
			LOGGER.warn("No Spring profile configured, running with default configuration");
		}
		else
		{
			LOGGER.info("Running with Spring profile(s) : {}", Arrays.toString(environment.getActiveProfiles()));
		}
	}

	@Bean
	public WebMvcConfigurer corsConfigurer()
	{
		return new WebMvcConfigurerAdapter()
		{
			@Override
			public void addCorsMappings(CorsRegistry registry)
			{
				registry.addMapping("/**")
						.allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH");
			}
		};
	}

	/**
	 * Main method, used to run the application.
	 */
	public static void main(String[] args) throws UnknownHostException
	{
		SpringApplication.run(Application.class, args);

	}
}
