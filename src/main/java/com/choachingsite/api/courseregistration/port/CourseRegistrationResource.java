package com.choachingsite.api.courseregistration.port;

import com.choachingsite.api.courseregistration.dto.ApiChargeRequest;
import com.choachingsite.api.courseregistration.service.CourseRegistrationService;
import com.choachingsite.api.courseregistration.dto.ApiCourseRegistration;
import com.choachingsite.api.courseregistration.service.StripeService;
import com.codahale.metrics.annotation.Timed;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping
public class CourseRegistrationResource
{
    private final CourseRegistrationService courseRegistrationService;
    private final StripeService paymentsService;

    @Autowired
    private CourseRegistrationResource(CourseRegistrationService courseRegistrationService, StripeService paymentsService)
    {
        this.courseRegistrationService = courseRegistrationService;
        this.paymentsService = paymentsService;
    }

    @RequestMapping(value = "/api/course/register",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> registerCourse(@Valid @RequestBody ApiCourseRegistration apiCourseRegistration) throws URISyntaxException,  StripeException
    {
        ApiChargeRequest apiChargeRequest = ApiChargeRequest.newBuilder()
                .currency(apiCourseRegistration.getPaymentCurrency())
                .description(apiCourseRegistration.getCourseId())
                .amount(apiCourseRegistration.getPaymentAmount())
                .stripeEmail(apiCourseRegistration.getStripeEmail())
                .stripeToken(apiCourseRegistration.getStripeToken())
                .build();

        Charge charge = paymentsService.charge(apiChargeRequest);

        if (charge.getStatus().equals("succeeded"))
        {
            courseRegistrationService.registerUserInCourse(apiCourseRegistration, charge);
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/course/charge",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> charge(@Valid @RequestBody ApiChargeRequest chargeRequest) throws URISyntaxException, StripeException
    {
        Charge charge = paymentsService.charge(chargeRequest);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(StripeException.class)
    public String handleError(Model model, StripeException ex) {
        model.addAttribute("error", ex.getMessage());
        return "result";
    }
}