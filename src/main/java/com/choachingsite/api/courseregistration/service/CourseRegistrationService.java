package com.choachingsite.api.courseregistration.service;

import com.choachingsite.api.course.repository.CourseRepository;
import com.choachingsite.api.user.service.UserService;
import com.choachingsite.api.courseregistration.domain.CourseRegistration;
import com.choachingsite.api.courseregistration.dto.ApiCourseRegistration;
import com.choachingsite.api.courseregistration.repository.CourseRegistrationRepository;
import com.stripe.model.Charge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CourseRegistrationService
{
    private final CourseRegistrationRepository courseRegistrationRepositoryRepository;
    private final UserService userService;
    private final CourseRepository courseRepository;

    @Autowired
    public CourseRegistrationService(CourseRegistrationRepository courseRegistrationRepositoryRepository,
                                     UserService userService,
                                     CourseRepository courseRepository)
    {
        this.courseRegistrationRepositoryRepository = courseRegistrationRepositoryRepository;
        this.userService = userService;
        this.courseRepository = courseRepository;
    }

    public void registerUserInCourse(ApiCourseRegistration apiCourseRegistration, Charge charge)
    {
        if (courseRepository.findCourseByCourseId(apiCourseRegistration.getCourseId()) == null)
        {
            throw new IllegalArgumentException("Course not found in the System");
        }

        Long userId = userService.createUser(apiCourseRegistration.getUser());
        CourseRegistration courseRegistration = CourseRegistration.newBuilder()
                .courseId(apiCourseRegistration.getCourseId())
                .userId(userId)
                .paymentChargeId(charge.getId())
                .paymentBalanceTransactionId(charge.getBalanceTransaction())
                .paymentFee(apiCourseRegistration.getPaymentAmount()/100.0) //since stripe takes values in cents
                .paymentStatus(charge.getStatus())
                .paymentCurrency(apiCourseRegistration.getPaymentCurrency())
                .paymentStripeToken(apiCourseRegistration.getStripeToken())
                .build();

        courseRegistrationRepositoryRepository.save(courseRegistration);
    }
}
