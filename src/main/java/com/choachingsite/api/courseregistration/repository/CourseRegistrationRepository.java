package com.choachingsite.api.courseregistration.repository;

import com.choachingsite.api.courseregistration.domain.CourseRegistration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRegistrationRepository extends JpaRepository<CourseRegistration, Long>
{

}
