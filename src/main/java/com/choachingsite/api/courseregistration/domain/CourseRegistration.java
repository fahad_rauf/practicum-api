package com.choachingsite.api.courseregistration.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_COURSE_REGISTRATION")
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = CourseRegistration.Builder.class)
public class CourseRegistration
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "course_id", nullable = false)
    private String courseId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "payment_status", nullable = false)
    private String paymentStatus;

    @Column(name = "payment_charge_id", nullable = false)
    private String paymentChargeId;

    @Column(name = "payment_balance_transaction_id", nullable = false)
    private String paymentBalanceTransactionId;

    @Column(name = "payment_fee", nullable = false)
    private Double paymentFee;

    @Column(name = "payment_currency", nullable = false)
    private String paymentCurrency;

    @Column(name = "payment_stripe_token", nullable = false)
    private String paymentStripeToken;


    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private CourseRegistration(Long id, String courseId, Long userId, String paymentStatus, String paymentChargeId,
                               String paymentBalanceTransactionId, Double paymentFee, String paymentCurrency, String paymentStripeToken)
    {
        this.id = id;
        this.courseId = courseId;
        this.userId = userId;
        this.paymentStatus = paymentStatus;
        this.paymentChargeId = paymentChargeId;
        this.paymentBalanceTransactionId = paymentBalanceTransactionId;
        this.paymentFee = paymentFee;
        this.paymentCurrency = paymentCurrency;
        this.paymentStripeToken = paymentStripeToken;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}