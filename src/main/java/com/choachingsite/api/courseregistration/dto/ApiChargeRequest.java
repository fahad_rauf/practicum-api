package com.choachingsite.api.courseregistration.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = ApiChargeRequest.Builder.class)
public class ApiChargeRequest
{
    private String description;
    private int amount;
    private String currency;
    private String stripeEmail;
    private String stripeToken;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private ApiChargeRequest(String description, int amount, String currency,
                             String stripeEmail, String stripeToken)
    {
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.stripeEmail = stripeEmail;
        this.stripeToken = stripeToken;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}
