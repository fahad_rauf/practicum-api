package com.choachingsite.api.courseregistration.dto;

import com.choachingsite.api.user.domain.User;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = ApiCourseRegistration.Builder.class)
public class ApiCourseRegistration
{
    private final User user;
    private final String courseId;
    private final String paymentStatus;

    private final int paymentAmount;
    private final String paymentCurrency;
    private final String stripeEmail;
    private final String stripeToken;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private ApiCourseRegistration(User user, String courseId, String paymentStatus, int paymentAmount,
                                  String paymentCurrency, String stripeEmail, String stripeToken)
    {
        this.user = user;
        this.courseId = courseId;
        this.paymentStatus = paymentStatus;
        this.paymentAmount = paymentAmount;
        this.paymentCurrency = paymentCurrency;
        this.stripeEmail = stripeEmail;
        this.stripeToken = stripeToken;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}
