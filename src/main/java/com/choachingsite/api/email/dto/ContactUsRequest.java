package com.choachingsite.api.email.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = ContactUsRequest.Builder.class)
public class ContactUsRequest
{
    private final String name;
    private final String subject;
    private final String emailAddress;
    private final String query;


    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private ContactUsRequest(String name, String subject, String emailAddress, String query)
    {
        this.name = name;
        this.subject = subject;
        this.emailAddress = emailAddress;
        this.query = query;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    { }
}
