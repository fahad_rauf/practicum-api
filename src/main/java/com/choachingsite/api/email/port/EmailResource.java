package com.choachingsite.api.email.port;

import com.choachingsite.api.email.dto.ContactUsRequest;
import com.choachingsite.api.email.service.EmailService;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.net.URISyntaxException;

@RestController
@RequestMapping
public class EmailResource
{
    private final Logger logger = LoggerFactory.getLogger(EmailResource.class);

    private EmailService emailService;
    private Environment env;
    private String adminEmail;

    @Autowired
    public EmailResource(EmailService emailService, Environment env)
    {
        this.emailService = emailService;
        this.env = env;
    }

    @PostConstruct
    public void init()
    {
        this.adminEmail = env.getProperty("spring.mail.username");
    }

    @RequestMapping(value = "/api/contact_us",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> sendUserQueryToAdmin(@Valid @RequestBody ContactUsRequest contactUsRequest) throws URISyntaxException
    {
        Assert.notNull(contactUsRequest, "REQUEST_IS_NULL");
        emailService.sendAdminUserQuery(contactUsRequest.getName(),
                contactUsRequest.getEmailAddress(), contactUsRequest.getSubject(), contactUsRequest.getQuery());
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/api/email",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> sendRegistrationEmail(@Valid @RequestBody ContactUsRequest contactUsRequest) throws URISyntaxException
    {
        Assert.notNull(contactUsRequest, "REQUEST_IS_NULL");
        emailService.sendEmail(contactUsRequest.getEmailAddress(),
                env.getProperty("spring.mail.username"), contactUsRequest.getQuery(), contactUsRequest.getSubject());
        return ResponseEntity.ok().build();
    }
}
