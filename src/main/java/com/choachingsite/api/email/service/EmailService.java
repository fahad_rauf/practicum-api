package com.choachingsite.api.email.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class EmailService
{
    private final Logger log = LoggerFactory.getLogger(EmailService.class);
    private AsyncMailService asyncMailService;
    private Environment env;
    private String adminEmail;

    @PostConstruct
    public void init()
    {
        this.adminEmail = env.getProperty("spring.mail.username");
    }

    @Autowired
    public EmailService(AsyncMailService asyncMailService,
                        Environment env)
    {
        this.env = env;
        this.asyncMailService  = asyncMailService;
    }

    /**
     *
     * @param name
     * @param emailAddress
     * @param subject
     * @param query
     */
    public void sendAdminUserQuery(String name, String emailAddress, String subject, String query)
    {
        String content = "";

        if(!name.equals(""))
        {
            content = "Name : " + name + " <br/>";
        }

        content += "Email Address : " + emailAddress + " <br/>";

        if(! subject.equals(""))
        {
            content+="Subject :  "+ subject+ " <br/>";
        }

        content += "  Query : " + query + " <br/>";
        sendEmail(adminEmail, adminEmail, content, subject);
    }

    public void sendEmail(String to, String from, String content, String subject)
    {
        log.debug("sending email '{}' to ",to);
        try{
            asyncMailService.sendEmail(to, from, subject, content, false, true);

        } catch(Exception e){
            log.warn("Sending Email Failed exception is : '{}'", e.getMessage());
        }
    }
}
