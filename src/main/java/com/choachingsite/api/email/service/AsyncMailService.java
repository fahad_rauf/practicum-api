package com.choachingsite.api.email.service;

import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class AsyncMailService
{
    private final Logger LOGGER = LoggerFactory.getLogger(AsyncMailService.class);

    private JavaMailSenderImpl javaMailSender;

    @Autowired
    AsyncMailService(JavaMailSenderImpl javaMailSender)
    {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendEmail(String to, String from, String subject, String content, boolean isMultipart, boolean isHtml) throws Exception
    {
        LOGGER.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try
        {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
            message.setTo(to.split(","));
            message.setFrom(from);
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            LOGGER.debug("Sent e-mail to User '{}'", to);
        }
        catch (Exception e)
        {
            LOGGER.error("E-mail could not be sent to user '{}' with subject {} and content {}, exception is: {}", to, subject, content, e.getMessage());
        }
    }
}
