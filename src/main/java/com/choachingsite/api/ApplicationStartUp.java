package com.choachingsite.api;

import com.choachingsite.api.config.Bootstrap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * This class is called when spring application context is loaded or refreshed
 */
@Component
public class ApplicationStartUp implements ApplicationListener<ContextRefreshedEvent>
{
    private final Bootstrap bootstrap;

    @Autowired
    ApplicationStartUp(Bootstrap bootstrap)
    {
        this.bootstrap = bootstrap;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event)
    {
        bootstrap.init();
    }
}
