package com.choachingsite.api.course.service;

import com.choachingsite.api.course.domain.CourseContent;
import com.choachingsite.api.course.domain.CourseDeliverable;
import com.choachingsite.api.course.domain.CourseFee;
import com.choachingsite.api.course.domain.Module;
import com.choachingsite.api.course.dto.ApiCourseContent;
import com.choachingsite.api.course.dto.ApiCourseModule;
import com.choachingsite.api.course.dto.ApiCourseResponse;
import com.choachingsite.api.course.repository.CourseContentRepository;
import com.choachingsite.api.course.repository.CourseDeliverableRepository;
import com.choachingsite.api.course.repository.CourseFeeRepository;
import com.choachingsite.api.course.repository.CourseRepository;
import com.choachingsite.api.course.repository.ModuleRepository;
import com.choachingsite.api.courseschedule.repository.CourseScheduleRepository;
import com.choachingsite.api.course.domain.Course;
import com.choachingsite.api.courseschedule.domain.CourseSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class CourseService
{
    private final CourseRepository courseRepository;
    private final CourseDeliverableRepository courseDeliverableRepository;
    private final CourseFeeRepository courseFeeRepository;
    private final CourseContentRepository courseContentRepository;
    private final CourseScheduleRepository courseScheduleRepository;
    private final ModuleRepository moduleRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository,
            CourseDeliverableRepository courseDeliverableRepository,
            CourseFeeRepository courseFeeRepository,
            CourseContentRepository courseContentRepository,
            CourseScheduleRepository courseScheduleRepository, ModuleRepository moduleRepository)
    {
        this.courseRepository = courseRepository;
        this.courseDeliverableRepository = courseDeliverableRepository;
        this.courseFeeRepository = courseFeeRepository;
        this.courseContentRepository = courseContentRepository;
        this.courseScheduleRepository = courseScheduleRepository;
        this.moduleRepository = moduleRepository;
    }

    public List<ApiCourseResponse> getAllCoursesDetails(String courseId)
    {
        if (courseId != null)
        {
            return Arrays.asList(this.getCourseDetails(courseId));
        }

        List<Course> courseList = courseRepository.findAll();
        List<ApiCourseResponse> courseResponseList = new ArrayList<>();
        courseList.forEach(course ->
        {
            List<CourseDeliverable> courseDeliverables = courseDeliverableRepository.findAllByCourseId(course.getCourseId());
            List<CourseFee> courseFees = courseFeeRepository.findAllByCourseId(course.getCourseId());
            List<CourseContent> courseContentList = courseContentRepository.findAllByCourseId(course.getCourseId());
            CourseSchedule courseSchedule = courseScheduleRepository.findOneByCourseId(course.getCourseId());
            List<ApiCourseContent> apiCourseContents = new ArrayList<>();
            courseContentList.forEach(courseContent -> {
                apiCourseContents.add(createApiCourseContent(courseContent));
            });
            courseResponseList.add(createApiCourseResponse(course, courseSchedule, courseDeliverables, courseFees, apiCourseContents));
        });

        return courseResponseList;
    }

    public ApiCourseResponse getCourseDetails(String courseId)
    {
        Course course = courseRepository.findCourseByCourseId(courseId);
        List<CourseDeliverable> courseDeliverables = courseDeliverableRepository.findAllByCourseId(courseId);
        List<CourseFee> courseFees = courseFeeRepository.findAllByCourseId(courseId);
        List<CourseContent> courseContents = courseContentRepository.findAllByCourseId(courseId);
        List<ApiCourseContent> apiCourseContents = new ArrayList<>();
        courseContents.forEach(courseContent -> {
            apiCourseContents.add(createApiCourseContent(courseContent));
        });
        CourseSchedule courseSchedule = courseScheduleRepository.findOneByCourseId(course.getCourseId());

        return createApiCourseResponse(course, courseSchedule, courseDeliverables, courseFees, apiCourseContents);
    }

    private ApiCourseContent createApiCourseContent (CourseContent courseContent)
    {
        List<Module> moduleList = moduleRepository.findAllByModuleId(courseContent.getModuleId());
        List<String> subModuleList = new ArrayList<>();
        String moduleTitle = moduleList.get(0).getModuleTitle();
        moduleList.forEach(module -> {
            subModuleList.add(module.getSubModuleTitle());
        });
        return ApiCourseContent.newBuilder()
                .courseId(courseContent.getCourseId())
                .subModules(subModuleList)
                .courseModule(moduleTitle)
                .build();
    }
    private ApiCourseResponse createApiCourseResponse(Course course, CourseSchedule courseSchedule, List<CourseDeliverable> courseDeliverables,
                                                     List<CourseFee> courseFees, List<ApiCourseContent> apiCourseContentList)
    {
        return ApiCourseResponse.newBuilder()
                .courseId(course.getCourseId())
                .courseTitle(course.getTitle())
                .courseCategory(course.getCategory())
                .courseDescription(course.getDescription())
                .courseOutline(course.getOutline())
                .courseContentInfo(course.getContentInfo())
                .courseFeeList(courseFees)
                .courseDeliverables(courseDeliverables)
                .courseContents(apiCourseContentList)
                .courseInstructor(courseSchedule.getInstructor())
                .courseLocation(courseSchedule.getLocation())
                .startDate(courseSchedule.getStartTime())
                .endDate(courseSchedule.getEndTime())
                .build();
    }
}