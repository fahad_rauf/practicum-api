package com.choachingsite.api.course.dto;

import com.choachingsite.api.course.domain.CourseContent;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = ApiCourseModule.Builder.class)
public class ApiCourseModule
{
    private final String courseId;
    private final String courseModule;
    private final List<String> subModules;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private ApiCourseModule(String courseId, String courseModule, List<String> subModules)
    {
        this.courseId = courseId;
        this.courseModule = courseModule;
        this.subModules = subModules;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    { }
}