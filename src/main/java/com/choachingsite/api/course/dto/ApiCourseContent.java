package com.choachingsite.api.course.dto;

import com.choachingsite.api.course.domain.CourseContent;
import com.choachingsite.api.course.domain.CourseDeliverable;
import com.choachingsite.api.course.domain.CourseFee;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = ApiCourseContent.Builder.class)
public class ApiCourseContent
{
    private final String courseId;
    private final String courseModule;
    private final List<String> subModules;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private ApiCourseContent(String courseId, String courseModule, List<String> subModules)
    {
        this.courseId = courseId;
        this.courseModule = courseModule;
        this.subModules = subModules;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    { }
}