package com.choachingsite.api.course.dto;

import com.choachingsite.api.course.domain.CourseContent;
import com.choachingsite.api.course.domain.CourseDeliverable;
import com.choachingsite.api.course.domain.CourseFee;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.time.LocalDateTime;
import java.util.List;

@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = ApiCourseResponse.Builder.class)
public class ApiCourseResponse
{
    private final String courseId;
    private final String courseTitle;
    private final String courseCategory;
    private final String courseDescription;
    private final String courseOutline;
    private final String courseContentInfo;
    private final List<CourseFee> courseFeeList;
    private final List<CourseDeliverable> courseDeliverables;
    private final List<ApiCourseContent> courseContents;
    private final LocalDateTime startDate;
    private final LocalDateTime endDate;
    private final String courseInstructor;
    private final String courseLocation;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private ApiCourseResponse(String courseId, String courseTitle, String courseCategory,
                              String courseDescription, String courseOutline, String courseContentInfo,
                              List<CourseFee> courseFeeList, List<CourseDeliverable> courseDeliverables,
                              List<ApiCourseContent> courseContents, LocalDateTime startDate, LocalDateTime endDate,
                              String courseInstructor, String courseLocation)
    {
        this.courseId = courseId;
        this.courseTitle = courseTitle;
        this.courseCategory = courseCategory;
        this.courseDescription = courseDescription;
        this.courseOutline = courseOutline;
        this.courseContentInfo = courseContentInfo;
        this.courseFeeList = courseFeeList;
        this.courseDeliverables = courseDeliverables;
        this.courseContents = courseContents;
        this.startDate = startDate;
        this.endDate = endDate;
        this.courseInstructor = courseInstructor;
        this.courseLocation = courseLocation;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    { }
}