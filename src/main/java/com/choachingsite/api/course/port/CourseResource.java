package com.choachingsite.api.course.port;

import com.choachingsite.api.course.dto.ApiCourseContent;
import com.choachingsite.api.course.dto.ApiCourseResponse;
import com.choachingsite.api.course.service.CourseService;
import com.codahale.metrics.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping
public class CourseResource
{
    private final CourseService courseService;

    @Autowired
    private CourseResource (CourseService courseService)
    {
        this.courseService = courseService;
    }

    @RequestMapping(value = "/api/course",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ApiCourseResponse> getAllCoursesDetails(@RequestParam(required = false) String courseId) throws URISyntaxException
    {
        return courseService.getAllCoursesDetails(courseId);
    }
}
