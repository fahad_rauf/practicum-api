package com.choachingsite.api.course.repository;

import com.choachingsite.api.course.domain.Course;
import com.choachingsite.api.course.domain.CourseContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseContentRepository extends JpaRepository<CourseContent, Long>
{
    List<CourseContent> findAllByCourseId(String courseId);
}
