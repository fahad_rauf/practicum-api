package com.choachingsite.api.course.repository;

import com.choachingsite.api.course.domain.CourseContent;
import com.choachingsite.api.course.domain.CourseDeliverable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseDeliverableRepository extends JpaRepository<CourseDeliverable, Long>
{
    List<CourseDeliverable> findAllByCourseId(String courseId);
}
