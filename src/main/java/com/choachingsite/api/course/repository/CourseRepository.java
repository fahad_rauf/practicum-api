package com.choachingsite.api.course.repository;

import com.choachingsite.api.course.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long>
{
    Course findCourseByCourseId (String courseId);
}
