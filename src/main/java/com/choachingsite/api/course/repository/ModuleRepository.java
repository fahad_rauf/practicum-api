package com.choachingsite.api.course.repository;

import com.choachingsite.api.course.domain.Course;
import com.choachingsite.api.course.domain.Module;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ModuleRepository extends JpaRepository<Module, Long>
{
    List<Module> findAllByModuleId(String moduleId);
}
