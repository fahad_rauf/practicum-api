package com.choachingsite.api.course.repository;

import com.choachingsite.api.course.domain.CourseDeliverable;
import com.choachingsite.api.course.domain.CourseFee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseFeeRepository extends JpaRepository<CourseFee, Long>
{
    List<CourseFee> findAllByCourseId(String courseId);
}
