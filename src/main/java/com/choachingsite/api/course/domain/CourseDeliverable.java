package com.choachingsite.api.course.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_COURSE_DELIVERABLE")
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = CourseDeliverable.Builder.class)
public class CourseDeliverable
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "course_id", nullable = false)
    private String courseId;

    @Column(name = "deliverable", nullable = false)
    private String deliverable;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private CourseDeliverable(Long id, String courseId, String deliverable)
    {
        this.id = id;
        this.courseId = courseId;
        this.deliverable = deliverable;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}