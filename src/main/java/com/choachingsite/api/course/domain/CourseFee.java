package com.choachingsite.api.course.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_COURSE_FEE")
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = CourseFee.Builder.class)
public class CourseFee
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "course_id", nullable = false)
    private String courseId;

    @Column(name = "fee", nullable = false)
    private Double fee;

    @Column(name = "fee_type", nullable = false)
    private String feeType;

    @Column(name = "fee_currency", nullable = false)
    private String feeCurrency;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private CourseFee(Long id, String courseId, Double fee, String feeType, String feeCurrency)
    {
        this.id = id;
        this.courseId = courseId;
        this.fee = fee;
        this.feeType = feeType;
        this.feeCurrency = feeCurrency;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}