package com.choachingsite.api.course.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_MODULE")
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = Module.Builder.class)
public class Module
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "module_id", nullable = false)
    private String moduleId;

    @Column(name = "module_title", nullable = false)
    private String moduleTitle;

    @Column(name = "sub_module_title", nullable = false)
    private String subModuleTitle;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private Module(Long id, String moduleId,
                   String moduleTitle, String subModuleTitle)
    {
        this.id = id;
        this.moduleId = moduleId;
        this.moduleTitle = moduleTitle;
        this.subModuleTitle = subModuleTitle;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}