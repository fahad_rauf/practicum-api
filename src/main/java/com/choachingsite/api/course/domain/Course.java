package com.choachingsite.api.course.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_COURSE")
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PACKAGE)
@JsonDeserialize(builder = Course.Builder.class)
public class Course
{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(name = "course_id", nullable = false, unique = true)
    private String courseId;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "category")
    private String category;

    @Column(name = "description", nullable = false, length = 100000)
    private String description;

    @Column(name = "outline", nullable = false)
    private String outline;

    @Column(name = "content_info", nullable = false)
    private String contentInfo;

    @lombok.Builder(builderClassName = "Builder", builderMethodName = "newBuilder", toBuilder = true)
    private Course(Long id, String courseId, String title, String category, String description, String outline, String contentInfo)
    {
        this.id = id;
        this.courseId = courseId;
        this.title = title;
        this.category = category;
        this.description = description;
        this.outline = outline;
        this.contentInfo = contentInfo;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class Builder
    {
    }
}